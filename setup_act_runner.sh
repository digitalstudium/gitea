#!/bin/bash

source .env

# Download and install act_runner
wget https://dl.gitea.com/act_runner/0.1.8/act_runner-0.1.8-linux-amd64 -O act_runner
chmod +x ./act_runner
sudo mv ./act_runner /usr/local/bin/

# Register server as runner on Gitea
act_runner register --instance https://$GITEA_HOSTNAME --token $GITEA_TOKEN --no-interactive

# Create act-runner systemd service and start it
sudo cp ./act-runner.service /lib/systemd/system/act-runner.service
sudo systemctl enable --now act-runner

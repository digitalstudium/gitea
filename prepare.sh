#!/bin/bash

# Update repo cache and install packages 
sudo apt update
sudo apt install -y nginx-light certbot python3-certbot-nginx docker.io

# Add current user to docker group and switch to this group
sudo usermod -a -G docker $USER
newgrp docker

# Init docker swarm mode if not yet initialized
docker swarm init || true

# Deploy docker stack
docker stack deploy -c stack.yaml gitea


#!/bin/bash

source .env

# Copy config to nginx, substitute  and reload it
sudo cp git.conf /etc/nginx/conf.d/
sudo sed -i s/GITEA_HOSTNAME/$GITEA_HOSTNAME/g /etc/nginx/conf.d/git.conf
sudo service nginx reload

# Get ssl certificate
sudo certbot --non-interactive --agree-tos -m $EMAIL


## Prerequisites

Before you begin, you will need:

- A server running Ubuntu 18.04 or later
- `sudo` privileges
- An internet connection

## Steps

1. Update the repository cache:

`sudo apt update`


2. Install the necessary packages:

`sudo apt install -y nginx-light certbot python3-certbot-nginx docker.io`


3. Add the current user to the `docker` group and switch to this group:

`sudo usermod -a -G docker $USER && newgrp docker`


4. Initialize Docker Swarm mode if it has not yet been initialized:

`docker swarm init || true`


5. Deploy the Gitea application using Docker Swarm:

`docker stack deploy -c stack.yaml gitea`

Note: The `stack.yaml` file contains the configuration for the Gitea application, including the Docker image to use, the ports to expose, and any environment variables to set.


6. Change `GITEA_HOSTNAME` and `EMAIL` inside `.env` file to your own values and then run `setup_nginx.sh` script:

`bash setup_nginx.sh`

It will generate nginx config and issue ssl certificate for your Gitea.

7. After that you can go to `$GITEA_HOSTNAME` URL and finish the installstion. At the bottom of install page, don't forget to create 
an administrator account.

8. Go to `$GITEA_HOSTNAME/admin/runners`, press `Create new runner` button, copy registration token from there and incert it into `.env` file as `GITEA_TOKEN` parameter.

9. Run `setup_act_runner` script, which will install CI agent to your node. You will se it on `$GITEA_HOSTNAME/admin/runners` page.